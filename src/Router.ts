import * as http from 'http';

interface IncomingMessageWithBody extends http.IncomingMessage {
    body?: any;
}

export class Router {
    private routes: { [key: string]: Handler<any> } = {};

    get<T>(path: string, handler: Handler<T>): this {
        if (this.routes[`GET:${path}`]) {
            throw new Error(`The handler for route GET:${path} has already been specified`);
        }

        this.routes[`GET:${path}`] = handler;
        return this;
    }

    post<T>(path: string, handler: Handler<T>): this {
        if (this.routes[`POST:${path}`]) {
            throw new Error(`The handler for route POST:${path} has already been specified`);
        }

        this.routes[`POST:${path}`] = handler;
        return this;
    }

    put<T>(path: string, handler: Handler<T>): this {
        if (this.routes[`PUT:${path}`]) {
            throw new Error(`The handler for route PUT:${path} has already been specified`);
        }

        this.routes[`PUT:${path}`] = handler;
        return this;
    }

    delete<T>(path: string, handler: Handler<T>): this {
        if (this.routes[`DELETE:${path}`]) {
            throw new Error(`The handler for route DELETE:${path} has already been specified`);
        }

        this.routes[`DELETE:${path}`] = handler;
        return this;
    }

    getHandler() {
        return async (request: IncomingMessageWithBody, response: http.ServerResponse) => {
            const method = request.method || '';
            const url = request.url || '';
            const handlerKey = `${method}:${url}`;

            const handler = this.routes[handlerKey];

            if (handler) {
                try {
                    if (method === 'POST' || method === 'PUT') {
                        const requestBody = await this.parseRequestBody(request);
                        request.body = requestBody;
                    }
                    await handler(request, response);
                } catch (error) {
                    this.handleError(response, 500, 'Internal Server Error');
                }
            } else {
                this.handleError(response, 404, 'Not Found');
            }
        };
    }

    private async parseRequestBody(request: http.IncomingMessage): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            let requestBody = '';

            request.on('data', (chunk) => {
                requestBody += chunk.toString();
            });

            request.on('end', () => {
                try {
                    const parsedBody = JSON.parse(requestBody);
                    resolve(parsedBody);
                } catch (error) {
                    reject(error);
                }
            });

            request.on('error', (error) => {
                reject(error);
            });
        });
    }

    private handleError(response: http.ServerResponse, statusCode: number, errorInfo: string) {
        console.error(errorInfo);
        response.setHeader('Content-Type', 'text/plain');
        response.statusCode = statusCode;
        response.end(errorInfo);
    }
}

export type Handler<T> = (request: http.IncomingMessage, response: http.ServerResponse) => Promise<void>;

export enum HTTPMethod {
    GET = 'GET',
    POST = 'POST',
    PUT = 'PUT',
    DELETE = 'DELETE'
}
